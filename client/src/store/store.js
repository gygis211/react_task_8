import { createStore, combineReducers, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga"

import { rootWatcher } from '../saga/index';
import homeReducer from "../reducers/home.reducer";
import newsListReducer from "../reducers/news.reducer";
import profileReducer from "../reducers/profile.reducer"

import userReduser from '../reducers/userReduser'

const sagaMiddleware = createSagaMiddleware()

export const rootReducer = combineReducers({
  home: homeReducer,
  profile: profileReducer,
  news: newsListReducer,
  userReduser
});

export const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootWatcher)
