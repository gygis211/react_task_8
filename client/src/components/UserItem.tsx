import React, { FC } from 'react';

export interface IUser {
    _id: number,
    name: string,
    surname: string,
    card: number
}

interface UserItemProps {
    user: IUser
    onClick: (user: IUser) => void
}

const UserItem: FC<UserItemProps> = ({user, onClick}) => {

    return (
        <div onClick={() => onClick(user)} style={{ padding: 15, border: '1px solid gray' }}>
            {user._id} {user.name} {user.surname} {user.card}
        </div>
    );
};

export default UserItem;