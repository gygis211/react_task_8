import React, { FC, useEffect, useState } from 'react';

import { useHistory } from 'react-router-dom';

import { IUser } from './UserItem';
import UserItem from './UserItem';
import List from './List';

const UserPage: FC = () => {
    const [users, setUsers] = useState<IUser[]>([])
    const history = useHistory()

    useEffect(() => {
        fetchUsers()
    }, [])

    async function fetchUsers() {
        try {
            const response = await fetch('http://localhost:5000/user/users',{
                method: "GET",
                headers: {
                    "Content-type": "application/json; charset = UTF-8",
                    },
            }).then(res => res.json())
            setUsers(response.data)
        } catch (error) {
            alert(error)
        }
    }

    return (
        <List
            items={users}
            renderItem={(user: IUser) =>
                <UserItem
                    onClick={(user) => history.push('/users/' + user._id)}
                    user={user}
                    key="{user._id}"
                />}
        />
    );
};

export default UserPage;