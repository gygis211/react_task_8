import React from 'react';
import { BrowserRouter, NavLink, Route } from 'react-router-dom'

import UserPage from './components/UserPage';

function App() {

  return (
    <BrowserRouter>
      <div>
        <div>
          <NavLink to='/users'>Пользователи</NavLink>
        </div>
          <Route path={'/users'} exact><UserPage /></Route>
      </div>
    </BrowserRouter>
      );
}

      export default App;
